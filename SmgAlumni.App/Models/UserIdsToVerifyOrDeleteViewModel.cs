﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmgAlumni.App.Models
{
    public class UserIdsToVerifyViewModel
    {
        public List<int> IdsToVerify { get; set; }
    }
}