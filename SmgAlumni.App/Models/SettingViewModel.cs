﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmgAlumni.App.Models
{
    public class SettingViewModel
    {
        public string SettingKey { get; set; }
        public string SettingValue { get; set; }
    }
}