﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmgAlumni.App.Models
{
    public class CauseNewsViewModes
    {
        public int Id { get; set; }
        public string Heading { get; set; }
        public string Body { get; set; }
    }
}