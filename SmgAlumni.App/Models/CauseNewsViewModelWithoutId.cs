﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmgAlumni.App.Models
{
    public class CauseNewsViewModelWithoutId
    {
        public string Heading { get; set; }
        public string Body { get; set; }
    }
}