﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmgAlumni.EF.Models.enums
{
    public enum ClassDivision
    {
        А,
        Б,
        В,
        Г,
        Д,
        Е,
        Ж, 
        З,
        И,
        Й,
        К
    }
}
